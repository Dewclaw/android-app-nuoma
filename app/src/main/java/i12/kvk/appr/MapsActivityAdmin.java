package i12.kvk.appr;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.time.Instant;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import i12.kvk.appr.adminFragments.CarHistoryFragment;
import i12.kvk.appr.adminFragments.UsersFragment;
import i12.kvk.appr.customerFragment.CustomerFragment;
import i12.kvk.appr.frag.PenaltiestFragment;

public class MapsActivityAdmin extends AppCompatActivity
        implements OnMapReadyCallback,
        NavigationView.OnNavigationItemSelectedListener,
        PenaltiestFragment.OnFragmentInteractionListener{

    public static GoogleMap mMap;
    SupportMapFragment mapFragment;
    AsyncTask asyncTask;
    TimerTask timerTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_admin);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_admin);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_map_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(55.700837, 21.143838), 15));
        /*
        * markerių atvaizdavimas ir atnaujinimas
        *
        * */
//        new GetUrlContentTask().execute("https://www.stops.lt/klaipeda/gps.txt?1548774463");
        markerUpdate();
        // Add a marker in Sydney, Australia, and move the camera.
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    private void markerUpdate() {
        Handler mainHandler = new Handler(Looper.getMainLooper());

        MapsActivityAdmin.this.runOnUiThread(() -> {

            timerTask = new TimerTask() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void run() {
                    Runnable myRunnable = () -> {
                        try {
                            GetUrlContentTask.cleanMap();
                        }catch (NullPointerException e){
                            e.printStackTrace();
                        }};
                    mainHandler.post(myRunnable);
//                    new GetUrlContentTask().execute("https://www.stops.lt/klaipeda/gps.txt?1548774463");
                    asyncTask = new GetUrlContentTask(MapsActivityAdmin.this).execute("https://www.stops.lt/klaipeda/gps.txt?"+ String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())));
                }
            };
            new Timer().schedule(timerTask,50,10000);
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MapsActivityAdmin.this, MapsActivity.class);
            startActivity(intent);
            timerTask.cancel();
//            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.map) {
            FrameLayout frameLayout = findViewById(R.id.frameloayt);
            frameLayout.setVisibility(View.INVISIBLE);



        } else if (id == R.id.users) {
            FrameLayout frameLayout = findViewById(R.id.frameloayt);
            frameLayout.setVisibility(View.VISIBLE);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frameloayt, CustomerFragment.newInstance())
                    .commit();
        } else if (id == R.id.car_history) {
            FrameLayout frameLayout = findViewById(R.id.frameloayt);
            frameLayout.setVisibility(View.VISIBLE);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frameloayt, CarHistoryFragment.newInstance())
                    .commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_map_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}