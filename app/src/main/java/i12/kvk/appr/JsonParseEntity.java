package i12.kvk.appr;

public class JsonParseEntity {
//    int first;
    String busNumber,busPlateNumber;
    double lon, lat;
//    int speed, direction;
//    String busPlateNumber;


    public JsonParseEntity(String busNumber, String busPlateNumber, double lon, double lat) {
        this.busNumber = busNumber;
        this.busPlateNumber = busPlateNumber;
        this.lon = lon;
        this.lat = lat;
    }
}
