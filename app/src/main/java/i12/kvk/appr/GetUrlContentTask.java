package i12.kvk.appr;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimerTask;

public class GetUrlContentTask extends AsyncTask<String, Integer, String> {
    private static final String TAG = "GetUrlContentTask";
    private static List<Marker> markers = new ArrayList<>();
    static GoogleMap map;
    Context context;
    public GetUrlContentTask(Context context) {
        this.context = context;
    }


    protected String doInBackground(String... urls) {
        try {
            URL url = new URL(urls[0]);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setConnectTimeout(50000);
            connection.setReadTimeout(50000);
            connection.connect();
            BufferedReader rd = new BufferedReader(new InputStreamReader( new BufferedInputStream(connection.getInputStream())));
            String content = "", line;
            while ((line = rd.readLine()) != null) {
                content += line + "\n";
            }
            return content;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onProgressUpdate(Integer... progress) {
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    protected void onPostExecute(String result) {
        // this is executed on the main thread after the process is over
        // update your UI here
//         displayMessage(result);
        String tst = "2,31,21091530,55863788,79,162,JZO 639 wm,";
        tst = tst.replaceFirst(".$", "");
        List<JsonParseEntity> jsonParseEntityList = new ArrayList<>();
        assert result != null;
        Log.d(TAG, "onPostExecute: Got Data "+ result);
        List<String> stringList = new ArrayList<>(Arrays.asList(result.split("\n")));

        for (String n : stringList) {

            List<String> strings = new ArrayList<>(Arrays.asList(n.split(",")));
            if (strings.size()>1) {
                if (strings.get(1).equals("5") || strings.get(1).equals("8") || strings.get(1).equals("14"))
                jsonParseEntityList.add(new JsonParseEntity(strings.get(1),strings.get(6),
                        Double.valueOf(strings.get(2).substring(0, 2) + "." + strings.get(2).substring(2)),
                        Double.valueOf(strings.get(3).substring(0, 2) + "." + strings.get(3).substring(2))
                ));
            }
        }

        Log.d(TAG, "onPostExecute: Content" + context);
        map = MapsActivity.mMap;
        if (map == null) {
            map = MapsActivityAdmin.mMap;

        }
        Log.d(TAG, "onPostExecute: Adding markers");
        for (JsonParseEntity js: jsonParseEntityList.subList(0, jsonParseEntityList.size())) {
            Marker marker = map.addMarker(new MarkerOptions()
                    .position(new LatLng(js.lat,js.lon))
                    .title(js.busNumber)
                    .icon(BitmapDescriptorFactory.fromBitmap(
                            new IconGenerator(context)
                                    .makeIcon(String.format("%s : %s",js.busNumber, js.busPlateNumber))
                    )));
            markers.add(marker);
        }
        /*.icon(BitmapDescriptorFactory.fromBitmap(
                new IconGenerator(context)
                        .makeIcon(String.format("%s\n%s",js.busNumber, js.busPlateNumber))))*/




//        LatLng latLng = new LatLng(jsonParseEntityList.get(4).lat, jsonParseEntityList.get(4).lon);
//        map.addMarker(new MarkerOptions().position(latLng));

        // Add a marker in Sydney, Australia, and move the camera.
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

    }

    public static List<Marker> getMarkers() {
        return markers;
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void cleanMap(){
        markers.forEach(Marker::remove);
//        map.clear();
    }
}