package i12.kvk.appr.orderHistoryFragment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import i12.kvk.appr.R;
import i12.kvk.appr.entity.OrderHistoryEntity;

public class OrderHistoryRecyclerAdapter extends
        RecyclerView.Adapter<OrderHistoryRecyclerAdapter.ViewHolder>{
    private static final String TAG = "OrderHistoryRecyclerAda";
    private List<OrderHistoryEntity> entityList;
    private Context context;

    public OrderHistoryRecyclerAdapter(Context context, List<OrderHistoryEntity> list) {
        this.entityList = list;
        this.context = context;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView car;
        public TextView price;
        public ImageView imgview;
        public TextView sTime;
        public TextView eTime;

        public View layout;
        public ViewHolder(View v) {
            super(v);
            layout = v;
            imgview = v.findViewById(R.id.imageView);
            car = v.findViewById(R.id.textCar);
            price = v.findViewById(R.id.textPrice);
            sTime = v.findViewById(R.id.dateStart);
            eTime = v.findViewById(R.id.dateEnd);

        }
    }

    public void add(int position, OrderHistoryEntity item) {
        entityList.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        entityList.remove(position);
        notifyItemRemoved(position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.fragment_order_history_row, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final OrderHistoryEntity name = entityList.get(position);
        Picasso.get()
                .load(name.getImg())
                .into(holder.imgview);
        holder.car.setText(String.format("%s %s", name.getBrand(), name.getModel()));
        holder.price.setText(String.format("Price : %s €",name.getCost()));
        holder.sTime.setText(name.getsTime());
        holder.eTime.setText(name.geteTime());



    }

    @Override
    public int getItemCount() {
        return entityList.size();
    }
}
