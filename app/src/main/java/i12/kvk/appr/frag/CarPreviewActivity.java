package i12.kvk.appr.frag;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import i12.kvk.appr.MapsActivity;
import i12.kvk.appr.R;

public class CarPreviewActivity extends MapsActivity {
    private static final String TAG = "CarPreviewActivity";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_car_preview);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_car_preview, null, false);
        drawer.addView(contentView, 0);
        Log.d(TAG, "onCreate: Started.");
        getIncomingData();
    }
    private void getIncomingData(){
        Log.d(TAG, "getIncomingData: checking for intented data");
        if (getIntent().hasExtra("data")){
            Log.d(TAG, "getIncomingData: dataFound");
            String[] data = getIntent().getStringArrayExtra("data");
            populateContextFields(data);
        }else Log.d(TAG, "getIncomingData: failed get data");
    }
    private void populateContextFields(String... args){
        Log.d(TAG, "populateContextFields: filling text fields and img");
        TextView description = findViewById(R.id.description),
                context = findViewById(R.id.context);
        ImageView image = findViewById(R.id.image);
        Picasso.get().load(args[args.length-1]).into(image);
        description.setText(String.format("Brand & Model : %s  %s",args[0], args[1]));
        context.setText(String.format(
                "Body type : %s       \nGearbox : %s \nPrice for a day : %s  \nPrice hour : %s",
                        args[2], args[3], args[4], args[5]));

    }
}
