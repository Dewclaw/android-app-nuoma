package i12.kvk.appr.carListPreview;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.Objects;
import i12.kvk.appr.R;

public class CarPreviewFragment extends Fragment {
    private static final String TAG = "CarPreviewFragment";
    TextView description, context;
    ImageView image;
    String[] data;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         data = Objects.requireNonNull(this.getArguments()).getStringArray("data");
        return inflater.inflate(R.layout.activity_car_preview, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        description = view.findViewById(R.id.description);
        context = view.findViewById(R.id.context);
        image = view.findViewById(R.id.image);
        getIncomingData();
        Log.d(TAG, "onViewCreated: OnViewCreated finished");
    }

    private void getIncomingData(){
        Log.d(TAG, "getIncomingData: checking for intended data");

        if ( getArguments().getStringArray("data") != null){
            Log.d(TAG, "getIncomingData: dataFound");
            populateContextFields(data);
        }else Log.d(TAG, "getIncomingData: failed get data");
    }
    private void populateContextFields(@NonNull String... args){
        Log.d(TAG, "populateContextFields: filling text fields and img");

        Picasso.get().load(args[args.length-1]).into(image);
        description.setText(String.format("Brand & Model : %s  %s",args[1], args[2]));
        context.setText(String.format(
                "Body type : %s       \nGearbox : %s \nPrice for a day : %s  \nPrice hour : %s",
                args[3], args[4], args[5], args[6]));
        Log.d(TAG, "populateContextFields: filling text fields and img FINISHED");
    }
}
