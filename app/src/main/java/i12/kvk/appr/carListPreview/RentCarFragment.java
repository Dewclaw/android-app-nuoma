package i12.kvk.appr.carListPreview;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import i12.kvk.appr.R;
import i12.kvk.appr.entity.EntityCars;

public class RentCarFragment extends Fragment {
    private static final String TAG = "RentCarFragment";
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    List<EntityCars> entityCarsList;
    String  page;

    public static RentCarFragment newInstance() {
        return new RentCarFragment();
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recycler_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycler_view);
        page = getResources().getString(R.string.ServerGetAllCars);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        entityCarsList = new ArrayList<>();
        loadUrlData();
        Log.d(TAG, "onViewCreated: created");
    }

    void loadUrlData(){
        RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(getActivity()));
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.show();


        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                page, response -> {
            progressDialog.dismiss();
            try {
                Log.d(TAG, "loadUrlData: Got the data from server");
                JSONArray array = new JSONArray(response);
                for (int i = 0; i < array.length(); i++){
                    JSONObject jo = array.getJSONObject(i);
                    EntityCars entityCars = new EntityCars(
                            jo.getString("car_id"),
                            jo.getString("brand"),
                            jo.getString("model"),
                            jo.getString("gearbox"),
                            jo.getString("body_type"),
                            jo.getString("price_day"),
                            jo.getString("price_hour"),
                            jo.getString("img_url")
                    );
                    entityCarsList.add(entityCars);
                }
                int en = entityCarsList.size();
                mAdapter = new RentCarRecyclerViewAdapter(getActivity(), entityCarsList);
                recyclerView.setAdapter(mAdapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> {
            progressDialog.setMessage(error.toString());
            Log.d("TAG", error.toString());
            Toast.makeText(getActivity(), "Error" + error.toString(), Toast.LENGTH_LONG).show();
        });
        requestQueue.add(stringRequest);
    }
}
