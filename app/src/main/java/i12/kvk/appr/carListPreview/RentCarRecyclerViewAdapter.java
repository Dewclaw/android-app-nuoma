package i12.kvk.appr.carListPreview;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import i12.kvk.appr.R;
import i12.kvk.appr.entity.EntityCars;

public class RentCarRecyclerViewAdapter extends RecyclerView.Adapter<RentCarRecyclerViewAdapter.ViewHolder> {
    private static final String TAG = "RentCarRecyclerAdapter";
    private List<EntityCars> entityCarsList;
    private Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtHeader;
        public TextView txtFooter;
        public ImageView imgview;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            imgview = v.findViewById(R.id.icon);
            txtHeader = v.findViewById(R.id.firstLine);
            txtFooter = v.findViewById(R.id.secondLine);
        }
    }

    public void add(int position, EntityCars item) {
        entityCarsList.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        entityCarsList.remove(position);
        notifyItemRemoved(position);
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RentCarRecyclerViewAdapter(Context context, List<EntityCars> myDataset) {
        entityCarsList = myDataset;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RentCarRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.recycler_view_row, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }



    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final EntityCars name = entityCarsList.get(position);
        holder.txtHeader.setText(String.format(
                "brand: %s model: %s ",
                name.getBrand(), name.getModel()));
        String[] strArr = {name.getId(), name.getBrand(), name.getModel(),name.getBody_type(), name.getGearbox(), name.getPrice_day(), name.getPrice_hour(), name.getImg()};
        holder.txtHeader.setOnClickListener(v -> {
            Log.d(TAG, "onBindViewHolder: Clicked on "+ entityCarsList.get(position));
            Bundle bundle = new Bundle();
            bundle.putStringArray("data",strArr);
            CarPreviewFragment previewFragment = new CarPreviewFragment();
            previewFragment.setArguments(bundle);

            FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
            manager.beginTransaction()
                    .replace(R.id.frameloayt, previewFragment)
                    .addToBackStack(null)
                    .commit();


//            Intent intent = new Intent(context, RecyclerViewActivity.class);
//            intent.putExtra("data",strArr);
//            context.startActivity(intent);

        });
        Picasso.get().load(name.getImg()).into(holder.imgview);
        holder.txtFooter.setText(String.format(
                "Footer:  price_day: %s price_hour: %s",
                name.getPrice_day(), name.getPrice_day()));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return entityCarsList.size();
    }

}