//package i12.kvk.appr.carListPreview;
//
//import android.app.ProgressDialog;
//import android.os.Bundle;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.widget.Toast;
//import com.android.volley.Request;
//import com.android.volley.RequestQueue;
//import com.android.volley.toolbox.StringRequest;
//import com.android.volley.toolbox.Volley;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import i12.kvk.appr.MapsActivity;
//import i12.kvk.appr.R;
//import i12.kvk.appr.entity.EntityCars;
//
//public class RecyclerViewActivity extends MapsActivity {
//    private static final String TAG = "RecyclerViewActivity";
//    private RecyclerView recyclerView;
//    private RecyclerView.Adapter mAdapter;
//    private RecyclerView.LayoutManager layoutManager;
//    List<EntityCars> entityCarsList;
//
//    final static String  page = "http://192.168.0.4:8080/api/cars";
////    final static String  url = "https://www.stops.lt/klaipeda/gps.txt?1548774463";
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
////        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
////        View contentView = inflater.inflate(R.layout.recycler_view, null, false);
////        drawer.addView(contentView, 0);
//        setContentView(R.layout.recycler_view);
//        recyclerView = findViewById(R.id.recycler_view);
//
//        // use this setting to improve performance if you know that changes
//        // in content do not change the layout size of the RecyclerViewActivity
//        recyclerView.setHasFixedSize(true);
//
//        // use a linear layout manager
//        layoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(layoutManager);
//
//
//        recyclerView.setAdapter(mAdapter);
//        entityCarsList = new ArrayList<>();
//        loadUrlData();
//    }
//
//    void loadUrlData(){
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        final ProgressDialog progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("Loading...");
//        progressDialog.show();
//
//
//            StringRequest stringRequest = new StringRequest(Request.Method.GET,
//                    page, response -> {
//                progressDialog.dismiss();
//                try {
//                    Log.d(TAG, "loadUrlData: Got the data from server");
//                    JSONArray array = new JSONArray(response);
//                    for (int i = 0; i < array.length(); i++){
//                        JSONObject jo = array.getJSONObject(i);
//                        EntityCars entityCars = new EntityCars(
//                                jo.getString("brand"),
//                                jo.getString("model"),
//                                jo.getString("body_type"),
//                                jo.getString("gearbox"),
//                                jo.getString("price_day"),
//                                jo.getString("price_hour"),
//                                jo.getString("img_url")
//                        );
//                        entityCarsList.add(entityCars);
//                    }
//                    int en = entityCarsList.size();
//                    mAdapter = new RentCarRecyclerViewAdapter(this, entityCarsList);
//                    recyclerView.setAdapter(mAdapter);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }, error -> {
//                progressDialog.setMessage(error.toString() + "\n"+ page);
//                Log.d("TAG", error.toString());
//                Toast.makeText(RecyclerViewActivity.this, "Error" + error.toString(), Toast.LENGTH_LONG).show();
//            });
//            requestQueue.add(stringRequest);
//    }
//}
//
