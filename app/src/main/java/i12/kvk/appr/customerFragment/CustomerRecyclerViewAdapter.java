package i12.kvk.appr.customerFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import i12.kvk.appr.R;
import i12.kvk.appr.entity.CustomersEntity;

public class CustomerRecyclerViewAdapter extends RecyclerView.Adapter<CustomerRecyclerViewAdapter.ViewHolder>{
    private static final String TAG = "CustomerRecyclerViewAda";
    private List<CustomersEntity> entityList;
    private Context context;

    public CustomerRecyclerViewAdapter(Context context, List<CustomersEntity> list) {
        this.entityList = list;
        this.context = context;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtHeader;
        public TextView txtFooter;
        public ImageView imgview;

        public View layout;
        public ViewHolder(View v) {
            super(v);
            layout = v;
            imgview = v.findViewById(R.id.icon);
            txtHeader = v.findViewById(R.id.firstLine);
            txtFooter = v.findViewById(R.id.secondLine);
        }
    }

    public void add(int position, CustomersEntity item) {
        entityList.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        entityList.remove(position);
        notifyItemRemoved(position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.fragment_user_recycler_row, viewGroup, false);

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final CustomersEntity name = entityList.get(position);
        holder.txtHeader.setText(String.format("Name : %s %s", name.getFname(), name.getLname()));
        holder.txtFooter.setText(String.format("Email : %s", name.getEmail()));
        Picasso.get().load("http://cdn.onlinewebfonts.com/svg/img_569204.png").into(holder.imgview);
        String[] strArr = {name.getId(), name.getEmail(), name.getFname(), name.getLname(), name.getCity(), name.getAddress(), name.getPhone(), "http://cdn.onlinewebfonts.com/svg/img_569204.png"};
        holder.txtHeader.setOnClickListener(v -> {
            Log.d(TAG, "onBindViewHolder: Clicked on "+ name);
            Bundle bundle = new Bundle();
            bundle.putStringArray("data", strArr);
            CustomerPreviewFragment previewFrament = new CustomerPreviewFragment();
            previewFrament.setArguments(bundle);
            FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
            manager.beginTransaction()
                    .replace(R.id.frameloayt, previewFrament)
                    .addToBackStack(null)
                    .commit();
        });

    }

    @Override
    public int getItemCount() {
        return entityList.size();
    }
}
