package i12.kvk.appr.customerFragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import i12.kvk.appr.R;


public class CustomerPreviewFragment extends Fragment {
    private static final String TAG = "CustomerPreviewFragment";
    ImageView image;
    TextView description, context;
    String[] data;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_customer_preview_frament, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        description = view.findViewById(R.id.description);
        context = view.findViewById(R.id.context);
        image = view.findViewById(R.id.image);
        getIncomingData();
    }

    private void getIncomingData() {
        Log.d(TAG, "getIncomingData: checking for incoming data");
        if (getArguments().getStringArray("data") != null){
            data = getArguments().getStringArray("data");
            Log.d(TAG, "getIncomingData: Data found");
            populateContextFields(data);
        }
    }

    private void populateContextFields(String... args) {
        Log.d(TAG, "populateContextFields: filling fields");
        Picasso.get().load(args[args.length-1]).into(image);
        description.setText(String.format("Name : %s         LastName : %s\n", args[2], args[3]));
        context.setText(String.format("Email : %s\nCity : %s, %s\nPhone : %s", args[1], args[4], args[5], args[6]));
    }
}
