package i12.kvk.appr.entity;

public class OrderHistoryEntity {
    String img, brand, model, cost, sTime, eTime;

    public OrderHistoryEntity(String img, String brand, String model, String cost, String sTime, String eTime) {
        this.img = img;
        this.brand = brand;
        this.model = model;
        this.cost = cost;
        this.sTime = sTime;
        this.eTime = eTime;
    }

    public String getImg() {
        return img;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getCost() {
        return cost;
    }

    public String getsTime() {
        return sTime;
    }

    public String geteTime() {
        return eTime;
    }
}
