package i12.kvk.appr.entity;

public class LoginEntity {
    private String customer_id, name, pass;

    public LoginEntity(String customer_id, String name, String pass) {
        this.customer_id = customer_id;
        this.name = name;
        this.pass = pass;
    }

    public LoginEntity(String name, String pass) {
        this.name = name;
        this.pass = pass;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public String getName() {
        return name;
    }

    public String getPass() {
        return pass;
    }
}
