package i12.kvk.appr.entity;

public class CustomersEntity {
    private String id, email, fname, lname, city, address, phone;

    public CustomersEntity(String id, String email, String fname, String lname, String city, String address, String phone) {
        this.id = id;
        this.email = email;
        this.fname = fname;
        this.lname = lname;
        this.city = city;
        this.address = address;
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }
}
