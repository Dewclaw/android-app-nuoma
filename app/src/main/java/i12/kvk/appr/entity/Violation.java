package i12.kvk.appr.entity;

public class Violation {
    String plateNumber, speed, lat, lon, date;

    public Violation(String plateNumber, String speed, String lat, String lon, String date) {
        this.plateNumber = plateNumber;
        this.speed = speed;
        this.lat = lat;
        this.lon = lon;
        this.date = date;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public String getSpeed() {
        return speed;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public String getDate() {
        return date;
    }
}
