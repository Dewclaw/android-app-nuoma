package i12.kvk.appr.entity;

public class EntityCars {
   private   String id, brand, model, gearbox, body_type, price_day, price_hour, img;

    public EntityCars(String id, String brand, String model, String gearbox, String body_type, String price_day, String price_hour, String img) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.gearbox = gearbox;
        this.body_type = body_type;
        this.price_day = price_day;
        this.price_hour = price_hour;
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getGearbox() {
        return gearbox;
    }

    public String getBody_type() {
        return body_type;
    }

    public String getPrice_day() {
        return price_day;
    }

    public String getPrice_hour() {
        return price_hour;
    }

    public String getImg() {
        return img;
    }
}
